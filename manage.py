import os
import subprocess
import contextlib
import sys

from flask_migrate import MigrateCommand
from flask_script import Manager, Command, Option

from marshaller import create_app, db
from marshaller.models import File, FileServer

manager = Manager(create_app)

manager.add_command('db', MigrateCommand)

root_path = os.path.dirname(os.path.abspath(__file__))

config_name = os.environ.get('LISTENUP_APP_CONFIG', 'development')


def start_debug(port):
    sys.path.append("pycharm-debug.egg")
    sys.path.append("pycharm-debug-py3k.egg")

    import pydevd
    pydevd.settrace('192.168.33.1', port=int(port), stdoutToServer=True, stderrToServer=True)


def _make_context():
    return dict(app=manager.app, db=db)


class CeleryWorker(Command):
    """Starts the celery worker."""
    name = 'celery'
    option_list = (
        Option('--debug', '-d', dest='debug', default=False),
    )

    def run(self, debug):
        if debug:
            start_debug(12346)
        ret = subprocess.call(
            ['celery', 'worker', '-A', 'marshaller.celery'])
        sys.exit(ret)


manager.add_command("celery", CeleryWorker())


@manager.option('-d', '--drop_first', help='Drop tables first?')
def createdb(drop_first=True):
    """Creates the database."""
    db.session.commit()
    if drop_first:
        print("Dropping all databases")
        db.drop_all()
    db.create_all()


@manager.command
def test():
    """Run unit tests"""
    tests = subprocess.Popen(['python', '-m', 'unittest'])
    tests.wait()
    with contextlib.suppress(FileNotFoundError):
        os.remove('files/test.db')


@manager.command
def resettobase():
    db.session.commit()
    db.drop_all()
    db.create_all()


@manager.option('-d', '--debug', dest='debug', default=False)
def statusing(debug):
    if debug:
        start_debug(12347)
    from marshaller.statusing import status
    status()


if __name__ == '__main__':
    manager.run()
