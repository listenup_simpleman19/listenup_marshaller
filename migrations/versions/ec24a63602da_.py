"""empty message

Revision ID: ec24a63602da
Revises: 1b5f93188cd8
Create Date: 2019-08-10 15:37:03.902603

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'ec24a63602da'
down_revision = '1b5f93188cd8'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.add_column('file_server', sa.Column('url', sa.String(length=100), nullable=False))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_column('file_server', 'url')
    # ### end Alembic commands ###
