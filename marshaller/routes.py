import os
from flask import Blueprint, request, current_app as app, g

from listenup_common.api_response import ApiException, ApiMessage, ApiResult
from marshaller.models import FileServer, File
from marshaller.tasks import  ping_servers
from . import db
import requests

main = Blueprint('main', __name__, url_prefix='/api/marshaller')

root_path = os.path.dirname(os.path.abspath(__file__))


@main.route('/files', methods=['GET'])
def all_files():
    files = File.query.all()
    return ApiResult(value=[file.to_dict() for file in files], status=200).to_response()


@main.route('/servers', methods=['GET'])
def all_servers():
    servers = FileServer.query.all()
    return ApiResult(value=[server.to_dict() for server in servers], status=200).to_response()


@main.route('/serversF', methods=['GET'])
def all_servers_with_files():
    servers = FileServer.query.all()
    return ApiResult(value=[server.to_dict(include_files=True) for server in servers], status=200).to_response()


# TODO handle type
@main.route('/upload/url')
def get_upload_url():
    servers = FileServer.query.all()
    if len(servers) > 0:
        return ApiResult(value={'url': servers[0].base_url + '/upload_file'}).to_response()
    else:
        raise ApiException(message="No Servers Found")


# TODO actually make a smart decision for primary and limit secondary to a handful
@main.route('/file/<string:guid>')
def get_file_url(guid):
    file = File.query.filter_by(file_guid=guid).one_or_none()
    if file:
        return ApiResult(value={
            'primary': file.servers[0].get_url(file.file_guid),
            'secondary': [server.get_url(file.file_guid) for server in file.servers],
        }, status=200).to_response()
    else:
        raise ApiException(message="Could not file file for:" + guid, status=404)


def health_check(url):
    try:
        return requests.get(url=url, timeout=5).status_code == 208
    except Exception as ex:
        print(str(ex))
        return False


@main.errorhandler(ApiException)
def handle_api_exception(error: ApiException):
    return error.to_result().to_response()
