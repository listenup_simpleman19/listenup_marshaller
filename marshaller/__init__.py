import os
from flask import Flask, request, session
from flask_migrate import Migrate
from flask_sqlalchemy import SQLAlchemy
from celery import Celery
from config import config

migrate = Migrate()

db = SQLAlchemy()

from marshaller import models

celery_broker = 'redis://:' + os.environ.get('REDIS_PASSWORD') + '@' + os.environ.get('REDIS') + '/1'

# TODO remove this it will print the password in plain text...
print(celery_broker)

celery = Celery(__name__,
                broker=celery_broker,
                backend=celery_broker)
celery.config_from_object('celeryconfig')

from marshaller import tasks

server_guid = None


def create_app(config_name=None):
    if config_name is None:
        config_name = os.environ.get('LISTENUP_APP_CONFIG', 'development')
        print("Starting up in: " + config_name)
    app = Flask(__name__)
    app.config.from_object(config[config_name])
    app.host = '0.0.0.0'

    celery.conf.update(config[config_name].CELERY_CONFIG)

    # Initialize flask extensions
    db.init_app(app)
    migrate.init_app(app=app, db=db)

    # Register web application routes
    from .routes import main as main_blueprint
    app.register_blueprint(main_blueprint)

    return app
