from marshaller import celery, db
from marshaller.models import File, FileServer
import feedparser
from time import mktime
from datetime import datetime
import os

RETRY_TIMES = 5

config_name = os.environ.get('LISTENUP_APP_CONFIG', 'development')


@celery.task
def ping_servers(feed_id):
    from marshaller import create_app
    app = create_app(config_name)
    with app.app_context():
        pass


if __name__ == '__main__':
    from marshaller import create_app
    app = create_app(config_name)
    with app.app_context():
        print("Updating feed: " + str(2))
