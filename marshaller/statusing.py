from typing import List
from flask import current_app as app
from . import db
from marshaller.models import FileServer, File
from marshaller.routes import health_check
from listenup_common.logging import get_logger
from listenup_common.wrappers.services import get_nodes
from listenup_common.wrappers.files import check_file
from datetime import datetime
import time
import requests
import os
import json

logger = get_logger(__name__)


def get_files_api_port(server):
    logger.info("Getting files api port from: %s", server.get('hostname', "Could not find hostname"))
    logger.debug("Filtering task_services from server")
    file_tasks = list(filter(lambda x: 'listenup_files_rp' in x['service']['name'], server['tasks_services']))
    logger.debug("Found %s tasks", str(len(file_tasks)))
    if len(file_tasks) == 1:
        ports = file_tasks[0]['service']['ports']
        filtered_ports = list(filter(lambda x: 80 == x['TargetPort'], ports))
        if len(filtered_ports) > 0:
            port = filtered_ports[0]['PublishedPort']
            logger.debug("Found ports: %s, returning: %s", filtered_ports, port)
            return port
        else:
            logger.error("Error filtering ports for file server api, found no ports")
            return None
    else:
        logger.error("Did not find the right task in %s", server)
        return None


def check_server(server: FileServer):
    logger.info("Checking server: " + str(server.id))
    try:
        resp = requests.get(server.base_url + '/server/stats')
        if resp.status_code == 200 and resp.json():
            resp_json = resp.json()
            server.load = resp_json['cpu']['percent_util']
            server.last_response_timestamp = datetime.utcnow()
            db.session.commit()
        resp = requests.get(server.base_url + '/files')
        if resp.status_code == 200 and resp.json():
            for file in resp.json():
                file_in_db = File.query.filter_by(file_guid=file.get("guid_file_name")).one_or_none()
                if not file_in_db:
                    file_in_db = File()
                    file_in_db.file_guid = file.get("guid_file_name")
                    db.session.add(file_in_db)
                if server not in file_in_db.servers:
                    file_in_db.servers.append(server)
                db.session.commit()
    except:
        logger.exception("Error with server id: " + str(server.id))  # Allow to continue if failed


def check_servers():
    logger.info("Checking servers")
    servers = FileServer.query.all()
    for server in servers:
        logger.info("Running health check on: %s", server.health_check)
        if health_check(server.health_check):
            logger.info("Server health check passed: %s", server.health_check)
            server.responding = True
            logger.info('Server: ' + server.base_url + ' is responding')
            check_server(server)
        else:
            logger.warn("Server health check failed: %s", server.health_check)
            if server.responding:
                logger.warn('Server: ' + server.base_url + ' didn\'t respond, flagging as not responding')
                # If previously responding the just set false and allow a retry
                server.responding = False
            else:
                logger.error('Server: ' + server.base_url + ' still not responding, killing it')
                # If already set false then this is the second failure and will kill it
                db.session.delete(server)
        db.session.commit()


def distribute_files():
    # TODO add distribution algo using locations and utilization instead of just min copies and a more random distribution
    min_copies = app.config.get("MINIMUM_COPIES_OF_FILE", 2)
    files = File.query.all()
    servers = FileServer.query.all()
    for file in files:
        if len(file.servers) < min_copies:
            uploaded = False
            for server in servers:
                if server not in file.servers:
                    if check_file(file.file_guid, server.url):
                        logger.warn("File: %s already existed on that server", file.file_guid)
                        uploaded = True
                    else:
                        uploaded = upload_file(file, server)
                        if uploaded:
                            break
            if not uploaded:
                if len(servers) < min_copies:
                    logger.error("Not enough servers to create enough copies of: %s", file.file_guid)
                logger.error("ERROR File: %s was not duplicated and only has: %s and min copies are: %s", file.file_guid, str(len(file.servers)), str(min_copies))
        else:
            logger.info("File: " + file.file_guid + " already has enough copies: " + str(len(file.servers))
                        + " and min copies are: " + str(min_copies))


def check_files():
    files: List[File] = File.query.all()
    logger.info("Checking for %s file existence on servers", str(len(files)))
    for file in files:
        try:
            logger.debug("Checking file: " + file.file_guid)
            for server in file.servers:
                found = check_file(file.file_guid, server.url)
                if not found:
                    logger.warn("Could not find file: %s on server: %s", file.file_guid, server.url)
                    file.servers.remove(server)
            if len(file.servers) == 0:
                logger.error("Could not find file: %s on any server so removing it", file.file_guid)
                db.session.delete(file)
        except Exception as ex:
            logger.exception("Error processing file: %s", file.file_guid)
    db.session.commit()


def upload_file(file: File, server: FileServer) -> bool:
    local_filename = file.file_guid
    temp_folder = app.config['TEMP_FOLDER']
    path_to_file = os.path.join(temp_folder, local_filename)
    if not os.path.exists(temp_folder):
        os.mkdir(temp_folder)
    from_server = file.servers[0]
    info = requests.get(from_server.get_info_url(file.file_guid)).json()
    # NOTE the stream=True parameter
    r = requests.get(from_server.get_url(file.file_guid), stream=True)
    with open(path_to_file, 'wb') as f:
        for chunk in r.iter_content(chunk_size=1024):
            if chunk:  # filter out keep-alive new chunks
                f.write(chunk)
    logger.info("Uploading file: %s to server: %s", path_to_file, server.base_url)
    files = {
        'file': (local_filename, open(path_to_file, 'rb'), {'Expires': '0'}),
        'info': (None, json.dumps(info), 'application/json')
    }
    url = server.base_url + '/internal_upload_file'
    response = requests.put(url, files=files)
    if response.status_code == 201 or response.status_code == 200:
        return True
    else:
        logger.error("Error uploading file: " + file.file_guid + " to: " + url)
    os.remove(path_to_file)
    return False


def detect_servers():
    logger.info("Detecting Servers")
    resp_json = get_nodes('labels=files:true')
    resp_string = str(resp_json)
    concat_resp_string = resp_string[:75] + (resp_string[75:] and '..')
    logger.info("Got response back from services of: %s", concat_resp_string)
    if resp_json:
        for server in resp_json:
            try:
                if server['state'] == 'ready':
                    logger.info("Server in ready state: %s", server['hostname'])
                    port = get_files_api_port(server)
                    if port:
                        # using IP from docker to make initial request as well as standard http and port from services
                        info = requests.get('http://' + server['ip'] + ':' + str(port) + '/api/files/info').json()
                        # other requests should be made using the info hostname and url
                        from_db = FileServer.query.filter_by(ip=server['ip']).one_or_none()
                        if not from_db:
                            from_db = FileServer()
                            from_db.ip = server['ip']
                        from_db.hostname = info.get('hostname')
                        from_db.url = info['url']
                        from_db.port = port
                        from_db.server_type = info['server_type']
                        from_db.health_check_endpoint = info['health_check_endpoint']
                        from_db.base_api_endpoint = info['base_url_endpoint']
                        db.session.add(from_db)
                    else:
                        logger.error("Could not get port for %s", server['hostname'])
            except Exception as ex:
                logger.exception("Failed to get info from server: %s", server['hostname'])
        db.session.commit()
    else:
        logger.error("Failed to get nodes from services")


def clean_unknown_files():
    files = File.query.all()
    for file in files:
        if len(file.servers) == 0:
            logger.error("ERROR - don't know where file: " + file.file_guid + " no servers attached to it, will remove from DB")
            db.session.delete(file)
    db.session.commit()


def status():
    logger.error("Statusing servers for marshaller")
    while True:
        try:
            detect_servers()
            check_servers()
            check_files()
            distribute_files()
            clean_unknown_files()
        except Exception as ex:
            # Try Again??
            logger.exception("Fatal error when trying to status servers", exc_info=True)
        time.sleep(app.config.get("STATUS_DELAY", 60))
