from marshaller import db
from datetime import datetime
from listenup_common.utils import guid


class BaseModel(db.Model):
    __abstract__ = True
    id = db.Column(db.Integer, primary_key=True)
    creation_timestamp = db.Column(db.DateTime, nullable=False,
                                   default=datetime.utcnow)


join_table = db.Table('file_to_file_server',
                      db.Column('file_server', db.Integer, db.ForeignKey('file_server.id')),
                      db.Column('file', db.Integer, db.ForeignKey('file.id'))
                      )


class FileServer(BaseModel):
    __tablename__ = 'file_server'
    server_guid = db.Column(db.String(32), nullable=False)
    url = db.Column(db.String(100), nullable=False)
    hostname = db.Column(db.String(100), nullable=False)
    ip = db.Column(db.String(100), nullable=False, default="")
    port = db.Column(db.Integer(), nullable=False)
    base_api_endpoint = db.Column(db.String(200), nullable=False)
    health_check_endpoint = db.Column(db.String(200), nullable=False)
    server_type = db.Column(db.String(50), nullable=False)
    free_space = db.Column(db.Float, default=-1, nullable=False)
    load = db.Column(db.Float, default=0, nullable=False)
    responding = db.Column(db.Boolean, default=True, nullable=False)
    last_response_timestamp = db.Column(db.DateTime, default=datetime.utcnow, nullable=False)
    files = db.relationship(
        "File",
        secondary=join_table,
        back_populates="servers"
    )

    def __init__(self):
        self.server_guid = guid()

    @property
    def base_url(self):
        return self.url + self.base_api_endpoint

    @property
    def health_check(self):
        return self.url + self.health_check_endpoint

    def to_dict(self, include_files=False):
        server_dict = {
            'id': self.id,
            'creation_timestamp': self.creation_timestamp,
            'server_guid': self.server_guid,
            'hostname': self.hostname,
            'ip': self.ip,
            'port': self.port,
            'base_url_endpoint': self.base_api_endpoint,
            'health_check_endpoint': self.health_check_endpoint,
            'free_space': self.free_space,
            'load': self.load,
            'responding': self.responding,
            'last_response_timestamp': self.last_response_timestamp,
            'server_type': self.server_type,
            'url': self.url,
        }
        if include_files:
            server_dict['files'] = [file.to_dict() for file in self.files]
        return server_dict

    def get_url(self, file_guid):
        return self.base_url + '/uploads/' + file_guid

    def get_info_url(self, file_guid):
        return self.base_url + '/file/' + file_guid


class File(BaseModel):
    __tablename__ = 'file'
    file_guid = db.Column(db.String(32), nullable=False)
    servers = db.relationship(
        "FileServer",
        secondary=join_table,
        back_populates="files"
    )

    def to_dict(self):
        return {
            'guid': self.file_guid,
            'urls': [server.get_url(self.file_guid) for server in self.servers],
            'server_guids': [server.server_guid for server in self.servers],
        }

